package com.studio.sunao.fususuapp.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;

public class BaseFragment extends Fragment {
    public static final String KEY_PAGE_TITLE = "KEY_PAGE_TITLE";
    private String mPageTitle;

    public static BaseFragment newInstance(String pageTitle){
        Bundle argument = new Bundle();
        argument.putString(KEY_PAGE_TITLE,pageTitle);
        BaseFragment baseFragment = new BaseFragment();
        baseFragment.setArguments(argument);
        return baseFragment;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Bundle argument = getArguments();
        mPageTitle = argument.getString(KEY_PAGE_TITLE);
    }

    public String getPageTitle() {
        return mPageTitle;
    }

    public void setPageTitle(String pageTitle) {
        mPageTitle = pageTitle;
    }

    protected BaseFragment getSelf() {
        return this;
    }

    public boolean onBack() {
        return true;
    };
}
